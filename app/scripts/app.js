var dashboardApp = angular.module('dashboardApp', ['ui.router']);

dashboardApp.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/main");
    //
    // Now set up the states
    $stateProvider
        .state('main', {
            url: "/main",
            templateUrl: "main.html",
            controller: "MainController"
        })
        .state('preference', {
            url: "/preference",
            templateUrl: "preference.html",
            controller: "PreferenceController"
        });
});


dashboardApp.factory('preferenceService', ['$log',
    function ($log) {
        $log.info("Initializing preferenceService...");
        return {
            doSomething: function () {
                $log.info("preference service invoked.");
            }
        };
    }]);

dashboardApp.factory('inventoryService', ['$log',
    function ($log) {
        $log.info("Initializing inventoryService...");
        return {
            doSomething: function () {
                $log.info("inventory service invoked.");
            }
        };
    }]);
