angular.module('dashboardApp').controller('PreferenceController', ['$scope', '$log', 'preferenceService',
    function ($scope, $log, preferenceService) {
        $log.info('PreferenceController initialized.');
        preferenceService.doSomething();
    }]);