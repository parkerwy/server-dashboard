angular.module('dashboardApp').controller('MainController', ['$scope', '$log', 'inventoryService', 'preferenceService',
    function ($scope, $log, inventoryService, preferenceService) {
        $log.info('MainController initialized.');
        inventoryService.doSomething();
    }]);